package com.test.application.hellowordvaadin;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class PinServiceImpl implements PingService {

	@Override
	public List<Result> getResult(File file){
		try{
			FileInputStream fileIs = new FileInputStream(file);
			HSSFWorkbook workbook = new HSSFWorkbook(fileIs);
			HSSFSheet sheet = workbook.getSheetAt(0);
			Stream<Row> rowStream = iteratorToStream(sheet.iterator());
			return rowStream.parallel().map(row -> mapRow(row)).collect(Collectors.toList());
		}catch(Exception e){
			e.printStackTrace();
		}
		return new ArrayList<Result>();
	}

	private static Result mapRow(Row row){
		Result result = new Result();
		iteratorToStream(row.cellIterator()).forEach(aCell -> 
					result.addCoumn(CellReference.convertNumToColString(aCell.getColumnIndex()), getValue(aCell)));
		return result;
		
	}
	private static  <T> Stream<T> iteratorToStream(Iterator<T> itreator){
		return StreamSupport.stream(
		          Spliterators.spliteratorUnknownSize(itreator, Spliterator.ORDERED),
		          false);
	}
	
	private static String getValue(Cell cell){
		switch(cell.getCellType()) {
		case Cell.CELL_TYPE_BOOLEAN:
			return String.valueOf(cell.getBooleanCellValue());
		case Cell.CELL_TYPE_NUMERIC:
			return String.valueOf(cell.getNumericCellValue());
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
		}
		return "";
	}
}
