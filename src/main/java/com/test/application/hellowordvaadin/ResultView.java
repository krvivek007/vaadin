package com.test.application.hellowordvaadin;

import java.io.File;
import java.util.List;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ResultView extends VerticalLayout implements View{
	Grid grid = new Grid();
	HorizontalLayout buttons = new HorizontalLayout();
	public ResultView() {
		Responsive.makeResponsive(this);
		grid.setColumns("net", "fqdn", "url", "ip", "port" );
		grid.setSizeFull();
		Button backButton = new Button("Back");

		backButton.addClickListener(new Button.ClickListener() {
		    public void buttonClick(ClickEvent event) {
		    	UI.getCurrent().getNavigator().navigateTo("form");
		    }
		});
		buttons.addComponents(backButton);
		Button export = new Button("Export");

		export.addClickListener(new Button.ClickListener() {
		    public void buttonClick(ClickEvent event) {
		        Notification.show("Do not press this button again");
		    }
		});
		buttons.addComponents(export);
		buttons.setMargin(new MarginInfo(false, true));
		addComponent(buttons);
		buttons.setHeightUndefined();

	}

	@Override
	public void enter(ViewChangeEvent event) {
		setSizeFull();
			setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		
		addComponents(grid);
		List<Result> results = new PinServiceImpl().getResult(
				(File)UI.getCurrent().getSession().getAttribute("file"));
		grid.setContainerDataSource(new BeanItemContainer<>(Result.class, results));
		setExpandRatio(buttons, 0.2f); 
		setExpandRatio(grid, 0.8f); 
		setMargin(true);
	}
}
