package com.test.application.hellowordvaadin;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class Result {
	private String net;
	private String fqdn;
	private String url;
	private String ip;
	private String port;
	private int rowNum;
	Map<String, String> allColumns = new HashMap<>();

	public void addCoumn(String key, String value){
		allColumns.put(key, value);
		if("A".equals(key)){
			net = value;
		}else if("B".equals(key)){
			fqdn = value;
		}else if("C".equals(key)){
			url = value;
		}else if("D".equals(key)){
			ip = value;
		}else if("E".equals(key)){
			try{
				port = String.valueOf((int)Double.parseDouble(value));
			}catch(NumberFormatException ex){

			}
		}
	}
}
