package com.test.application.hellowordvaadin;

import java.io.File;
import java.util.List;

public interface PingService {
	List<Result> getResult(File file);
}
