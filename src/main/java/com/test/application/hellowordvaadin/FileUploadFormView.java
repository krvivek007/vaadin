package com.test.application.hellowordvaadin;

import java.io.File;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class FileUploadFormView extends VerticalLayout implements View{

    public FileUploadFormView() {
        setSizeFull();
        Component fileForm = buildForm();
        addComponent(fileForm);
        setComponentAlignment(fileForm, Alignment.MIDDLE_CENTER);

        Notification notification = new Notification("Welcome to Sanitation App");
        notification.setDescription("<span>Use this app to check firewall status</span>");
        notification.setHtmlContentAllowed(true);
        notification.setStyleName("tray dark small closable login-help");
        notification.setPosition(Position.BOTTOM_CENTER);
        notification.setDelayMsec(20000);
        notification.show(Page.getCurrent());
        
        File uploads = new File("/tmp/uploads");
        if (!uploads.exists() && !uploads.mkdir()){
        	 new Notification("ERROR: Could not create upload dir",  Notification.Type.ERROR_MESSAGE).show(Page.getCurrent());
        }
    }

    private Component buildForm() {
        Panel panel = new Panel("Sanitation Tool");
        panel.setSizeUndefined();
        final HorizontalLayout fileForm = new HorizontalLayout();
        fileForm.setSizeUndefined();
        fileForm.setSpacing(true);
        fileForm.addStyleName("login-panel");
        fileForm.addComponent(buildLabels());
        fileForm.addComponent(buildFields());
        fileForm.setMargin(true);
        panel.setContent(fileForm);
        return panel;
    }

    private Component buildFields() {
        FileUploader uploaer = new FileUploader();
        Upload upload = new Upload();
        upload.setReceiver(uploaer);
        upload.addSucceededListener(uploaer);
        upload.setImmediate(true);
        upload.setButtonCaption("Check Network");
        return upload;
    }

    private Component buildLabels() {
        CssLayout labels = new CssLayout();
        labels.addStyleName("labels");
        Label fileLabel = new Label("Choose file:");
        fileLabel.setSizeUndefined();
        labels.addComponent(fileLabel);
        return labels;
    }
    
    @Override
    public void enter(ViewChangeEvent event) {
        Notification.show("Welcome to the Sanitation Tool");
    }

}
