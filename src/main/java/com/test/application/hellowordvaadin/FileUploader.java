package com.test.application.hellowordvaadin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.UUID;

import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;


@SuppressWarnings("serial")
class FileUploader implements Receiver, SucceededListener {
	public File file;
    public OutputStream receiveUpload(String filename, String mimeType) {
        FileOutputStream fos = null; // Stream to write to
        try {
        	file = new File("/tmp/uploads/" + UUID.randomUUID());
            fos = new FileOutputStream(file);
        } catch (final java.io.FileNotFoundException e) {
            new Notification("Could not open file\n",
                             e.getMessage(),
                             Notification.Type.ERROR_MESSAGE)
                .show(Page.getCurrent());
            return null;
        }
        return fos;
    }

    public void uploadSucceeded(SucceededEvent event) {
    	UI.getCurrent().getSession().setAttribute("file", file);
    	UI.getCurrent().getNavigator().navigateTo("result");
    }
    
    public void uploadSFailed(FailedEvent event) {
        System.out.println("file uploaded...");
     }
};